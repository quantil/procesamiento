import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="procesamiento",
    version="0.0.1",
    author="David Delgado",
    author_email="david.delgado@quantil.com.co",
    description="Processing package for CAC ERC data",
    packages=['procesamiento', 'test'],
    #packages=setuptools.find_packages(),
    scripts=['bin/etl.py'],
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "pandas==0.23.4",
        "pyodbc==4.0.24",  
    ], 
)