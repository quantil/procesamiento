# procesamiento


Contiene los scripts que:

1. Extraen los datos de la base de datos SQL Server 2012 de la CAC.
2. Limpian, procesan los datos extraídos a través de un ETL.
4. Ejecutan el modelo y capturan el resultado del mismo.
3. Insertan los datos procesados en la base de datos PostgreSQL.


### Prerequisitos

* Python 3.6, pandas 0.23.4, pyodbc 4.0.24

### Instalación

1. Clone el repositorio localmente (`git clone <url_repo>`).
2. Cree y configure el modulo de configuración (`procesamiento/config.py`) de acuerdo a las instrucciones que encontrará a continuación.
3. Instale el paquete localmente (`pip install <ruta_local_clon>`). Se recomienda fuertemente trabajar dentro de un ambiente virtual.

### Configuración
En un módulo de Python con ruta `procesamiento/config.py` usted debe especificar las siguientes estructuras de datos:

```python
# Diccionario que contiene los parámetros de configuración a la base de datos.
# La lista de columnas debe contener los campos 'FECHA_HECHO','HORA','LONGITUD_X','LATITUD_Y' Y 'DIA_SEMANA'.
db = {
    'user':'user_ej',                               # nombre de usuario en la base de datos 
    'pass':'pass_ej',                               # contraseña en la base de datos
    'host':'host_ej',                               # dirección, puerto y ruta donde escucha la base de datos
    'tabla':'tabla_ej',                             # tabla que contiene la información
}
```
```python
# Diccionario que contiene los códigos y datos de los municipios de Colombia.
# FALTA EDITAR
cod_municipios = {
    '01':'Usaquén',                                 # localidad 01 - Usaquén
    '02':'Chapinero',                               # localidad 02 - Chapinero
    '03':'Santa Fe',                                # localidad 03 - Santa Fe
    '04':'San Cristóbal',                           # localidad 04 - San Cristóbal
    '05':'Usme',                                    # localidad 05 - Usme
    '06':'Tunjuelito',                              # localidad 06 - Tunjuelito
    '07':'Bosa',                                    # localidad 07 - Bosa
    '08':'Kennedy',                                 # localidad 08 - Kennedy
    '09':'Fontibón',                                # localidad 09 - Fontibón
    '10':'Engativa',                                # localidad 10 - Engativa
    '11':'Suba',                                    # localidad 11 - Suba
    '12':'Barrios Unidos',                          # localidad 12 - Barrios Unidos 
    '13':'Teusaquillo',                             # localidad 13 - Teusquillo
    '14':'Los Mártires',                            # localidad 14 - Los Mártires
    '15':'Antonio Nariño',                          # localidad 15 - Antonio Nariño
    '16':'Puente Aranda',                           # localidad 16 - Puente Aranda
    '17':'La Candelaria',                           # localidad 17 - La Candelaria
    '18':'Rafael Uribe Uribe',                      # localidad 18 - Rafael Uribe Uribe
    '19':'Ciudad Bolivar',                          # localidad 19 - Ciudad Bolivar
    '20':'Sumapaz'                                  # localidad 20 - Sumapaz
}
```

